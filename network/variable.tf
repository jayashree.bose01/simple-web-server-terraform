#---------------networking/variables.tf

variable "vpc_cidr" {}

variable "pub_sub_cidrs" {
    type = list
}

variable "access_ip" {}