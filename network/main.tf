#----------networking/main.tf---------------

#---check availabllity zone------------

data "aws_availability_zones" "available" {
    state = "available"
}


#-----vpc--------

resource "aws_vpc" "terraform_vpc" {
  cidr_block = var.vpc_cidr
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true
  
  tags = {
      name = "tf_vpc"
  }
}


#---internet gateway-------

resource "aws_internet_gateway" "terraform_internet_gateway" {
    vpc_id = aws_vpc.terraform_vpc.id
    
    tags = {
        name = "tf_igw"
    }
}


#--------subnets-------

resource "aws_subnet" "terraform_public_subnet" {
    count = length(var.pub_sub_cidrs)

    vpc_id = aws_vpc.terraform_vpc.id

    cidr_block = var.pub_sub_cidrs[count.index]
    map_public_ip_on_launch = true
    availability_zone = data.aws_availability_zones.available.names[count.index]
    
    tags = {
        name = "tf_pub_sub${count.index + 1}"
    }
}

#------route tables------

resource "aws_route_table" "terraform_public_rt" {
    vpc_id = aws_vpc.terraform_vpc.id
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.terraform_internet_gateway.id
    }
    
    tags = {
        name = "tf_public_rt"
    }
}


#---------------route table association-------

resource "aws_route_table_association" "public" {
    count = length(aws_subnet.terraform_public_subnet)
    subnet_id = aws_subnet.terraform_public_subnet.*.id[count.index]
    route_table_id = aws_route_table.terraform_public_rt.id
}


#----------------network ACL------
resource "aws_network_acl" "terraform_public_nacl" {
    vpc_id = aws_vpc.terraform_vpc.id
    
    ingress {
        protocol = "tcp"
        rule_no = 100
        action = "allow"
        cidr_block = var.access_ip
        from_port = 80
        to_port = 80
    }
    
    egress {
        protocol = "tcp"
        rule_no = 100
        action = "allow"
        cidr_block = var.access_ip
        from_port = 80
        to_port = 80
    }

    ingress {
        protocol = "tcp"
        rule_no = 120
        action = "allow"
        cidr_block = var.access_ip
        from_port = 1024
        to_port = 65535
    }

    ingress {
        protocol = "tcp"
        rule_no = 110
        action = "allow"
        cidr_block = var.access_ip
        from_port = 22
        to_port = 22
    }
    
    egress {
        protocol = "tcp"
        rule_no = 110
        action = "allow"
        cidr_block = var.access_ip
        from_port = 1024
        to_port = 65535
    }
    
    #count = length(aws_subnet.terraform_public_subnet)
    subnet_ids = aws_subnet.terraform_public_subnet.*.id


    #subnet_ids = ["subnet-0004fc5ac4ba23e3b","subnet-000d0a289f3d9b25d"]
    
    tags = {
        name = "tf_public_nacl"
    }
}

#---------------security groups--------

resource "aws_security_group" "terraform_public_sg" {
    name = "public_sg"
    description = "allow outside traffic to access the web server(EC2 instance.)"
    vpc_id = aws_vpc.terraform_vpc.id
    
    #SSH
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.access_ip]
    }
    
    #--custom tcp

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [var.access_ip]
    }
    
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [var.access_ip]
    }
}