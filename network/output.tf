#----------networking/output.tf---------------

output "pub_cidrs" {
    value = "${aws_subnet.terraform_public_subnet.*.cidr_block}"
}

output "pub_security_group" {
    value = "${aws_security_group.terraform_public_sg.id}"
}

output "public_subnet_id" {
    value = "${aws_subnet.terraform_public_subnet.*.id}"
}

output "vpc_id" {
    value = aws_vpc.terraform_vpc.id
}