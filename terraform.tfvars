aws_region = "ap-south-1"

#-----networking-----

vpc_cidr = "10.20.0.0/16"

pub_sub_cidrs = [
    "10.20.1.0/24",
    "10.20.2.0/24"
    ]

access_ip = "0.0.0.0/0"   


#------compute---

key_name = "tf_key"

instance_count = 2

instance_type = "t2.micro"
