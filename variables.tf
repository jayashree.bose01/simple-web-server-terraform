#-------variables.tf-----

variable "aws_region" {}

#----- networking variable------

variable "vpc_cidr" {}
variable "pub_sub_cidrs" {
    type = list
}
variable "access_ip" {}


#---------compute variable----------

variable "key_name" {}

variable "instance_count" {
    default = 1
}
variable "instance_type" {}
