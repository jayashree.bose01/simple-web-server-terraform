#--------------compute/main.tf--------


#-------aws-ami-search--------

data "aws_ami" "terraform_ami_search" {
    owners = ["amazon"]
    most_recent = true
    
    filter {
        name = "name"
        values = ["amzn2-ami-hvm*-x86_64-gp2"] 
    }
}


#----public key----

resource "aws_key_pair" "ec2_key_pair" {

    key_name = var.key_name
    public_key = file("${path.module}/id_rsa.pub")
}


#----web server script------

data "template_file" "user-init" {
    count    = 2
    template = file("${path.module}/userdata.sh")
   
    vars = {
        firewall_subnets = element(var.pub_cidrs, count.index)
    }
    
}

#---EC2 instance----

resource "aws_instance" "terraform_web_server" {

    count = var.instance_count

    instance_type = var.instance_type
    ami = data.aws_ami.terraform_ami_search.id
    
    key_name = aws_key_pair.ec2_key_pair.key_name
    
    vpc_security_group_ids = [var.security_group]
    subnet_id = element(var.public_subnet_id , count.index)
    associate_public_ip_address = true
    user_data = data.template_file.user-init.*.rendered[count.index]

    #user_data = data.template_file.user-init.rendered
    
    tags = {
        name = "tf_web_server${count.index + 1}"
    }
}


#---- application load balancer

resource "aws_lb" "terraform_app_lb" {
 
  name               = "terraform-app-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.security_group]
  subnets            = var.public_subnet_id
  idle_timeout       = 400

  tags = {
    Name = "tf-app-lb"
  }
}

#------ target group---------

resource "aws_lb_target_group" "terraform_aap_tg" {
  name        = "terraform-aap-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}


#----- load balancer listener---------

resource "aws_lb_listener" "terraform_alb_listener" {
  load_balancer_arn = aws_lb.terraform_app_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.terraform_aap_tg.arn
  }
}


#------target group attachment----

resource "aws_lb_target_group_attachment" "terraform_tg_attachment" {
  count = 2
  target_group_arn = aws_lb_target_group.terraform_aap_tg.arn
  target_id        = element(aws_instance.terraform_web_server.*.id, count.index)
  port             = 80
}
