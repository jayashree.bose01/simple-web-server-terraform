#--------------compute/variables.tf--------

variable "key_name" {}

variable "vpc_id" {}

variable "pub_cidrs" {
    type = list
}

variable "instance_count" {}

variable "instance_type" {}

variable "security_group" {}

variable "public_subnet_id" {
    type = list
}