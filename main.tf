#----main.tf-----

provider "aws" {

  region  = var.aws_region
  access_key = "AKIA6JKOOLBNQTYPZJUJ"
  secret_key = "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"
}


#----deploy networking resource

module "terraform_network" {

    source = "./network"
    vpc_cidr = var.vpc_cidr
    pub_sub_cidrs = var.pub_sub_cidrs
    access_ip = var.access_ip
}


#---------- deploy compute module

module "terraform_compute" {
    source = "./compute"
    key_name = var.key_name
    vpc_id = module.terraform_network.vpc_id
    instance_count = var.instance_count
    instance_type = var.instance_type
    pub_cidrs = module.terraform_network.pub_cidrs
    security_group = module.terraform_network.pub_security_group
    public_subnet_id = module.terraform_network.public_subnet_id
}
